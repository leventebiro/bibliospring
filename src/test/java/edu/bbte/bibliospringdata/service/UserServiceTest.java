package edu.bbte.bibliospringdata.service;

import edu.bbte.bibliospringdata.model.User;
import edu.bbte.bibliospringdata.repository.UserRepository;
import edu.bbte.bibliospringdata.util.PasswordEncrypter;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.BDDMockito.given;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import static org.junit.jupiter.api.Assertions.*;
import static org.hamcrest.MatcherAssert.assertThat;

import static org.awaitility.Awaitility.given;

@SpringBootTest
public class UserServiceTest {
    @Autowired
    private UserService userService;
    @MockBean
    private UserRepository userRepository;
    @MockBean
    private PasswordEncrypter passwordEncrypter;

    private User dbUser;
    private User loggedInUser;
    private User wrongPassworsUser;
    private User notExistUser;


    @BeforeEach
    public void setup() {
        dbUser = new User();
        dbUser.setUserName("Ubulka");
        dbUser.setPassword("ubulka");

        loggedInUser = new User();
        loggedInUser.setUserName("Ubulka");
        loggedInUser.setPassword("ubulka");

        wrongPassworsUser = new User();
        wrongPassworsUser.setUserName("Ubulka");
        wrongPassworsUser.setPassword("ubul");

        notExistUser = new User();
        notExistUser.setUserName("Jozsi");
        notExistUser.setPassword("ubul");
    }

    @Test
    public void testLogin() {
        //given
        given(userRepository.findByUserName("Ubulka")).willReturn(dbUser);
        given(passwordEncrypter.generatedHashedPassword(eq("ubulka"), anyString())).willReturn("ubulka");
        //when
        boolean exitingUserGoodPassword = userService.login(loggedInUser);
        boolean exitingUserWrongPassword = userService.login(wrongPassworsUser);
        boolean wrongUser = userService.login(notExistUser);

        //then
       assertTrue(exitingUserGoodPassword);
       assertFalse(exitingUserWrongPassword);
       assertFalse(wrongUser);
    }
}
