package edu.bbte.bibliospringdata;

import edu.bbte.bibliospringdata.model.User;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;


import java.util.ArrayList;


import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.jupiter.api.Assertions.*;

public class SimpleTest {
    @Test
    public void testAdd() {
        assertEquals(42, Integer.sum(21, 21));
    }

    @Test
    public void testForException() {
        ArrayList arrayList = new ArrayList();
        assertThrows(IndexOutOfBoundsException.class, () -> arrayList.get(0));
    }

    private User user1;
    private User user2;

    @BeforeEach
    public void setup() {
        user1 = new User();
        user2 = new User();
        user2.setUid(user1.getUid());
    }

    @Test
    public void testWithHamcest() {
        assertThat("Equals user objects", user1, equalTo(user2));
    }
}
