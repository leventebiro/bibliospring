package edu.bbte.bibliospringdata.repository;

import edu.bbte.bibliospringdata.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {
    User findByUserName(String username);

    @Query("FROM User u where u.userName=:username")
    User mindegyNevu(@Param("username") String username);
}