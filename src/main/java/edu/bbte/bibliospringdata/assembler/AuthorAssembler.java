package edu.bbte.bibliospringdata.assembler;

import edu.bbte.bibliospringdata.dto.incoming.AuthorInDTO;
import edu.bbte.bibliospringdata.dto.outgoing.AuthorOutDTO;
import edu.bbte.bibliospringdata.model.Author;
import org.springframework.stereotype.Component;

@Component
public class AuthorAssembler {

    public Author authorINDtoToModel(AuthorInDTO authorInDTO) {
        Author author = new Author();
        author.setFirstName(authorInDTO.getFirstname());
        author.setLastName(authorInDTO.getLastname());
        author.setDateOfBirth(authorInDTO.getDateOfBirth());
        return author;
    }

    public AuthorOutDTO modelToAuthorOutDto(Author author) {
        AuthorOutDTO authorOutDTO = new AuthorOutDTO();
        authorOutDTO.setFirstname(author.getFirstName());
        authorOutDTO.setLastname(author.getLastName());
        authorOutDTO.setDateOfBirth(author.getDateOfBirth());
        return authorOutDTO;
    }
}
