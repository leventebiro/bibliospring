package edu.bbte.bibliospringdata.api;

import edu.bbte.bibliospringdata.assembler.BookAssembler;
import edu.bbte.bibliospringdata.dto.incoming.BookInDTO;
import edu.bbte.bibliospringdata.dto.outgoing.BookOutDTO;
import edu.bbte.bibliospringdata.model.Author;
import edu.bbte.bibliospringdata.model.Book;
import edu.bbte.bibliospringdata.service.AuthorService;
import edu.bbte.bibliospringdata.service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/books")
public class BookController {

    @Autowired
    BookService bookService;
    @Autowired
    AuthorService authorService;
    @Autowired
    BookAssembler bookAssembler;

    @GetMapping()
    public List<BookOutDTO> getBooksByTitleContains(@RequestParam(name = "keyword", required = false) String keyword) {
        if (keyword == null) {
            ArrayList<BookOutDTO> bookOutDTOArrayList = new ArrayList<>();
            List<Book> books = bookService.getAll();
            for (Book book : books) {
                bookOutDTOArrayList.add(bookAssembler.modelToBookOutDto(book));
            }
            return bookOutDTOArrayList;
        } else {
            List<Book> books = bookService.findByTitleContains(keyword);
            return books.stream()
                    .map(bookAssembler::modelToBookOutDto)
                    .collect(Collectors.toList());
        }
    }

    @PostMapping()
    public ResponseEntity<BookOutDTO> addBook(@RequestBody BookInDTO bookInDTO) {
        Author author = new Author();
        author.setFirstName(bookInDTO.getAuthor().getFirstName());
        author.setLastName(bookInDTO.getAuthor().getLastName());
        author.setDateOfBirth(bookInDTO.getAuthor().getDateOfBirth());

        Author savedAuthor = authorService.create(author);

        Book book = new Book();
        book.setTitle(bookInDTO.getTitle());
        book.setPublishedYear(bookInDTO.getPublishedYear());
        book.setGenre(bookInDTO.getGenre());
        book.setAuthor(savedAuthor);
        book.setAuthorID(savedAuthor.getID());

        Book savedBook = bookService.create(book);

        BookOutDTO bookOutDTO = bookAssembler.modelToBookOutDto(savedBook);
        URI uri = URI.create("/books/" + bookOutDTO.getId());
        return ResponseEntity.created(uri).body(bookOutDTO);
    }
}
