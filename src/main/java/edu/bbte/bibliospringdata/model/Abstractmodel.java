package edu.bbte.bibliospringdata.model;

import jakarta.persistence.Column;
import jakarta.persistence.MappedSuperclass;
import jakarta.persistence.PrePersist;

import java.util.Objects;
import java.util.UUID;
@MappedSuperclass
public abstract class Abstractmodel {
    @Column(name ="uid")
    private String uid;

    public String getUid() {
       ensureUid();
        return uid;
    }
    private void ensureUid(){
        if(uid == null){
            uid = UUID.randomUUID().toString();
        }
    }
    @PrePersist
    public void prePersist(){
        ensureUid();
    }
    public void setUid(String uid) {
        this.uid = uid;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        Abstractmodel that = (Abstractmodel) obj;
        return Objects.equals(getUid(), that.getUid());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getUid());
    }
    @Override
    public String toString() {
        return uid +" ";
    }
}
