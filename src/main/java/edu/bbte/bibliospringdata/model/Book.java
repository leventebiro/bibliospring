package edu.bbte.bibliospringdata.model;


import jakarta.persistence.*;

@Entity
@Table(name = "books")
public class Book extends BaseEntity {
    @Column(name = "title")
    private String title;
    @ManyToOne
    @JoinColumn(name = "authorid", insertable = false, updatable = false)
    private Author author;
    @Column(name = "publishedYear")
    private Integer publishedYear;
    @Column(name = "genre")
    private String genre;
    @Column(name = "authorID")
    private Long authorId;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }


    public Author getAuthor() {
        return author;
    }

    public void setAuthor(Author author) {
        this.author = author;
    }

    public Integer getPublishedYear() {
        return publishedYear;
    }

    public void setPublishedYear(Integer publishedYear) {
        this.publishedYear = publishedYear;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    @Override
    public String toString() {
        return super.toString() + title + "-" + author + " " + publishedYear + " " + genre;
    }

    public Long getAuthorId() {
        return authorId;
    }
    public void setAuthorID(Long authorId) {
        this.authorId = authorId;
    }
}
