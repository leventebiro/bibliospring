package edu.bbte.bibliospringdata.model;


import jakarta.persistence.*;

@MappedSuperclass
public abstract class BaseEntity extends Abstractmodel {
    @Column(name ="id")
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long ID;

    public Long getID() {
        return ID;
    }

    public void setID(Long ID) {
        this.ID = ID;
    }
    @Override
    public String toString() {
        return super.toString() +ID + " ";
    }
}
